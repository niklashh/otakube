# Temporary otakube

## Device connection

On host execute

```shell
sudo k3s kubectl port-forward octotemp-0 1234:1234 &
socat -dd /dev/ttyACM0 TCP:localhost:1234,keepalive,reuseaddr
```
