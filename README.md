# Otakube

## Server

A box from hetzner with an EXT4 volume (from hetzner too) attached to it.

## Steps

1. Provision the hosts
1. ```shell
   ansible-playbook -i hosts.yml --private-key=[KEY LOCATION] -K playbook.yml
   ```
1. Input password for sudo

## Roles

### Wireguard

Generates server and client configs to /etc/wireguard on the host

Vars:

- Specify an IP address for the server's wireguard interface in `wg_private_ip`
- Specify the client in `wg_user_list` with each user (peer) having
  - An arbitrary unique `username`
  - An IP address `private_ip`
  - Option to remove the user (explicitly removes the user folder from /etc/wireguard)

### K3s

Installs k3s on the host

Vars:

- K3s version to download `k3s_version`
- Unit file directory `systemd_dir`

### UFW

Sets a basic set of firewall rules. Only allows access to k3s through the wireguard vpn
