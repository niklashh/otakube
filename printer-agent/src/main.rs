use std::path::PathBuf;
use udev::Enumerator;

mod config;
mod socket;

#[derive(Debug)]
struct Device<'d> {
    name: &'d str,
    node: PathBuf,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut en = Enumerator::new()?;
    en.match_subsystem("tty")?;
    let devs = en.scan_devices()?;

    let devpaths = devs
        .filter_map(|dev| {
            let parent = dev
                .parent_with_subsystem_devtype("usb", "usb_device")
                .unwrap()?;
            let value = parent.attribute_value("devpath")?.to_owned();
            let name = dev.devnode()?.to_owned();
            Some((name, value))
        })
        .collect::<Vec<_>>();

    for boxed in &devpaths {
        let (path, devpath) = boxed;
        println!("{:?} has devpath {:?}", path, devpath);
    }

    let config = config::open(PathBuf::from("./config.toml"));
    let devices: Vec<Device> = devpaths
        .iter()
        .filter_map(|(path, devpath)| {
            let printer = config
                .printers
                .iter()
                .find(|&p| p.devpath == devpath.to_str().unwrap())?;
            Some(Device {
                name: &printer.name[..],
                node: path.to_owned(),
            })
        })
        .collect();

    println!("{:?}", devices);

    socket::main().await.unwrap();

    Ok(())
}
