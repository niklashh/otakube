use std::path::PathBuf;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub printers: Vec<Printer>
}

#[derive(Deserialize, Debug)]
pub struct Printer {
    pub name: String,
    pub devpath: String,
}

pub fn open(path: PathBuf) -> Config {
    let file = std::fs::read_to_string(path).unwrap();
    let config: Config = toml::from_str(&file).unwrap();
    config
}
