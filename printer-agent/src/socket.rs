use tokio::io::split;
use tokio::net::TcpStream;
use tokio::prelude::*;
use tokio_serial::SerialPort;

pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let stream = TcpStream::connect("127.0.0.1:1234").await?;
    let (mut read_server, mut write_server) = split(stream);

    // let file = File::open("/dev/ttyACM0").await?;
    let  settings = tokio_serial::SerialPortSettings::default();
    let mut port = tokio_serial::Serial::from_path(std::path::PathBuf::from("/dev/ttyACM0"), &settings).unwrap();
    port.set_baud_rate(250_000)?;
    
    let (mut read_file, mut write_file) = split(port);

    // file -> server
    let fut1 = async move {
        loop {
            let mut buf = [0; 1024];

            let n = read_file.read(&mut buf[..]).await.unwrap();
            if n > 0 {
                println!("{}", String::from_utf8_lossy(&buf[..n]));
                write_server.write_all(&buf[..n]).await.unwrap();                
            }
        }
    };

    // server -> file
    let fut2 = async move {
        loop {
            let mut buf = [0; 1024];

                let n = read_server.read(&mut buf[..]).await.unwrap();
                if n>0 {
//                    let s = String::from_utf8_lossy(&buf[..n]);
                    write_file.write_all(&buf[..n]).await.unwrap();                
                }
        }
    };

     tokio::join!(tokio::spawn(fut1), tokio::spawn(fut2));
  //  tokio::join!(tokio::spawn(fut2)).0.unwrap();
    Ok(())
}
